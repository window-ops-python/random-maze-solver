# random-maze-solver

This is a algorithm that solves a random maze.

## Installation

To run this project, you will need to have Python, numpy, random,matplotlib and queue installed on your computer. You can download Python from the official website, and install the dependencies using pip:

```
pip install numpy, random, matplotlib, queue
```

## Usage

To run the project, simply run the `app.py` file using Python:

```
python app.py
```
